const data = require("./GSAFilePlan.out.json");
window._data = data;

// TODO refine search to return a number > 0 and <= 1 for relevance
// TODO refine search to match case-insensitive
// TODO refine search to match across multiple fields
const matches = search => record => {
  return (record.Description || "")
    .toLowerCase()
    .includes(search.toLowerCase());
};

const { fromEvent, from, merge } = rxjs;
const { map, tap, filter, distinctUntilChanged, debounceTime } = rxjs.operators;

const txtSearch = document.getElementById("repo-search");
const btnSearch = document.getElementById("do-search");
const resultList = document.getElementById("results");

const searches = merge(
  fromEvent(btnSearch, "click"),
  fromEvent(txtSearch, "input")
).pipe(
  map(e => txtSearch.value),
  filter(s => s.length > 2),
  distinctUntilChanged(),
  debounceTime(700),
  map(text => ({
    type: "search/start",
    payload: { text }
  }))
);

const { Agent } = AntaresProtocol;
const agent = new Agent();

// Consequence 0: logging
agent.addFilter(({ action }) => console.log(action));

// Consequence 1: clear out div
agent.on("search/start", ({ action }) => (resultList.innerHTML = ""));

// Consequence 2: each match becomes a search result
// prettier-ignore
agent.on("search/start", ({ action }) => {
    const { text } = action.payload;
    const matching = data.filter(matches(text));
    return from(
      [...matching.map(m => ({
        type: "search/result",
        payload: m
      })), {
        type: "search/complete",
        payload: {count: matching.length}
      }]
    );
  },
  {
    processResults: true
  }
);

// Consequence 3: a search result gets an element in the list
agent.on("search/result", ({ action }) => {
  const { Description } = action.payload;
  if (!Description) return;

  resultList.innerHTML += `
  <li class="list-group-item d-flex justify-content-between align-items-center">
      ${Description}
  </li>
  `;
});

// Consequence 4: show how many matched
agent.on("search/complete", ({ action }) => {
  const { count } = action.payload;
  resultList.innerHTML =
    `<li class="list-group-item d-flex justify-content-between align-items-center">${count} records matching
  </li>` + resultList.innerHTML;
});

// Initiate the program - the agent will process a new search/start when user search changes.
agent.subscribe(searches);
