<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
      integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
      crossorigin="anonymous"
    />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rxjs/6.2.1/rxjs.umd.min.js"></script>
    <script src="https://unpkg.com/antares-protocol@2.9.0/dist/antares-protocol.js"></script>
    <title>GSA Search</title>
  </head>
  <body>
    <div class="container">
      <div class="row"><h1>GSA Search (In Development alpha)</h1></div>
      <div class="row">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon3"
              >Policies matching:</span
            >
          </div>
          <input
            type="text"
            class="form-control"
            id="repo-search"
            aria-describedby="basic-addon3"
          />
          <div class="input-group-append">
            <button
              class="btn btn-outline-secondary"
              type="button"
              id="do-search"
            >
              Go
            </button>
          </div>
        </div>
      </div>
      <div class="row">
        <ul class="list-group" id="results" style="width: 100%">
          <li
            class="list-group-item d-flex justify-content-between align-items-center"
          >
            Your results will appear here..
          </li>
        </ul>
      </div>
    </div>
  </body>
</html>
