
# The GSA Project

Data: https://docs.google.com/spreadsheets/d/141-p9G06fbr8TY5b3tXcVyzBgjW5PSkP5C1-9UbfP3A/edit#gid=0


Today's Increment: Deploy a new version with fields and layout like those here:
https://drive.google.com/open?id=0B0QMqE0wOhugazhxOTdQS2dWSFhqNlFTRHZGVVNwX2QzTUVZ

0. Get current layout on the operating table:
  - live reloading
  - able to build and deploy

1. Make changes for layout
1. Make changes for Antares
1. Deploy and verify
1. Create screenshots, invoice

In another increment
1. Go through and document the build/deploy process
