const HtmlWebpackPlugin = require("html-webpack-plugin");
const HtmlWebpackInlineSourcePlugin = require("html-webpack-inline-source-plugin");

module.exports = {
  entry: __dirname + "/index.js",
  output: {
    path: __dirname + "/dist",
    filename: "index_bundle.js"
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "GSA Search",
      filename: "gsa-search-webpack.html",
      inlineSource: ".(js|css)$",
      template: "livesearch.tpl"
    }),
    new HtmlWebpackInlineSourcePlugin()
  ]
};
